``` ini

BenchmarkDotNet=v0.13.1, OS=Windows 7 SP1 (6.1.7601.0)
AMD Athlon(tm) X4 840 Quad Core Processor, 1 CPU, 4 logical and 2 physical cores
Frequency=3019814 Hz, Resolution=331.1462 ns, Timer=TSC
.NET SDK=5.0.401
  [Host]     : .NET Core 3.1.0 (CoreCLR 4.700.19.56402, CoreFX 4.700.19.56404), X64 RyuJIT  [AttachedDebugger]
  DefaultJob : .NET Core 3.1.0 (CoreCLR 4.700.19.56402, CoreFX 4.700.19.56404), X64 RyuJIT


```
|           Method |       Mean |    Error |   StdDev |
|----------------- |-----------:|---------:|---------:|
|    OldCheckByReg | 1,440.4 ns | 14.39 ns | 13.46 ns |
| OldCheckManually |   300.5 ns |  5.88 ns |  6.77 ns |
|       CheckByReg | 1,075.6 ns | 11.48 ns | 10.17 ns |
|    CheckManually |   115.0 ns |  1.76 ns |  1.65 ns |
