``` ini

BenchmarkDotNet=v0.13.1, OS=Windows 7 SP1 (6.1.7601.0)
AMD Athlon(tm) X4 840 Quad Core Processor, 1 CPU, 4 logical and 2 physical cores
Frequency=3019824 Hz, Resolution=331.1451 ns, Timer=TSC
.NET SDK=5.0.401
  [Host]     : .NET Core 3.1.0 (CoreCLR 4.700.19.56402, CoreFX 4.700.19.56404), X64 RyuJIT  [AttachedDebugger]
  DefaultJob : .NET Core 3.1.0 (CoreCLR 4.700.19.56402, CoreFX 4.700.19.56404), X64 RyuJIT


```
| Method |     Mean |    Error |   StdDev |
|------- |---------:|---------:|---------:|
| Sha256 | 71.89 μs | 0.330 μs | 0.309 μs |
|    Md5 | 25.57 μs | 0.138 μs | 0.129 μs |
