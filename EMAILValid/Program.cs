﻿using System;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace EMAILValid
{
    public class EmailComparer
    {
        string email = "schukin1990@rambler.ru";
        Regex oldMailReg;
        Regex mailReg;
        public EmailComparer()
        {
            oldMailReg = new Regex(@"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                   RegexOptions.IgnoreCase);

            var rand = (new Random());

            email = new string('s', rand.Next(5, 20)) + "@" +
                new string('r', rand.Next(2, 10)) + "." +
                new string('r', rand.Next(2, 5));

            mailReg = new Regex(@"^[^@\s]+?@[^@\s]+?\.[^@\s]+?$",
                   RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        [Benchmark]
        public bool OldCheckByReg()
        {
            return oldMailReg.IsMatch(email);
        }

        [Benchmark]
        public bool OldCheckManually()
        {
            var res = false;
            var parts = email.Split("@");

            if (parts.Length == 2)
                if (!parts[0].Contains(" ") && parts[0].Length > 0
                    && !parts[1].Contains(" ") && parts[1].Length > 3)
                {
                    var domainParts = parts[1].Split(".");
                    if (domainParts.Length == 2
                        && domainParts[0].Length > 0 && domainParts[1].Length > 1)

                        res = true;
                }


            return res;
        }
        [Benchmark]
        public bool CheckByReg()
        {
            return mailReg.IsMatch(email);
        }

        [Benchmark]
        public bool CheckManually()
        {
            var res = false;
            var emailSpan = new Span<char>(email.ToCharArray());
            var dogIndex = emailSpan.IndexOf("@");
            if (dogIndex > 0 && emailSpan.Length > (dogIndex + 1))
            {
                var partFirst = emailSpan.Slice(0, dogIndex);
                var partSecond = emailSpan.Slice(dogIndex + 1);

                if (partSecond.IndexOf("@") == -1)
                    if (partFirst.IndexOf(" ") == -1
                        && partSecond.IndexOf(" ") == -1 && partSecond.Length > 3)
                    {
                        var dotIndex = partSecond.IndexOf(".");
                        if (dotIndex > 0 && partSecond.Length > (dotIndex + 1))
                        {
                            var domainPartFirst = partSecond.Slice(0, dotIndex);
                            var domainPartSecond = partSecond.Slice(dotIndex + 1);

                            if (domainPartSecond.IndexOf(".") == -1)
                                if (domainPartSecond.Length > 1)
                                    res = true;
                        }
                    }
            }

            return res;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<EmailComparer>();
        }

    }
}
